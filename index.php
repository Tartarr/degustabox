<?php

$username = "marc";
$password = "degustabox";	

// Create connection
$db = mysqli_connect("localhost",$username,$password,"degustabox");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
//get info of total day work
$date = date('Y-m-d');
$sql = "SELECT * FROM tracker WHERE day = '$date'";
$result = $db->query($sql);
$tasks = mysqli_num_rows($result);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Exercise "Time tracker"</title>
	<!-- load jquery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/js.js"></script>
	<link rel="stylesheet" href="css/styles.css">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
</head>
<body>
	<div class="container" class="m10t">

		<label>Name of the task
			<input type="text" name="task" id="task">
		</label>
		<button type="button" class="btn btn-success" id="starter">START</button>
		<button type="button" class="btn btn-danger" id="stopper" style="display:none;">STOP</button>
		<div id="feedback-alert" class="text-center alert"></div>
		<div id="count"></div>
	</div>
	<div class="container m40t">
		<div style="overflow-x:auto;width:100%;">
			<div>
				<span class="col head">Task</span>
				<span class="col head">Duration</span>
			</div>
			<?php
			if ($tasks > 0){
				$total = 0;
				while($row = $result->fetch_assoc()){
					$h = floor($row['duration'] / 3600);
					$min = floor(($row['duration'] - ($h * 3600)) / 60);
					$s = $row['duration'] - ($h * 3600) - ($min * 60);
					$duracion = $h.'h '.$min.'min '.$s.'s';

					$total += $row['duration'];
					?>
					<div>
						<span class="col"><?php echo $row['task']; ?></span>
						<span class="col"><?php echo $duracion ?></span>
					</div>
				<?php 
				} 
				$ht = floor($total / 3600);
				$mint = floor(($total - ($ht * 3600)) / 60);
				$st = $total - ($ht * 3600) - ($mint * 60);
				$duracion_total = $ht.'h '.$mint.'min '.$st.'s';
				?>
				<div>
					<span class="col">TOTAL</span>
					<span class="col"><?php echo $duracion_total ?></span>
				</div>
				<?php
			}else{ ?>
				<div class="m10t msg">
					No tasks yet today
				</div>
<?php 
			}
				?>
			</table>
		</div>
		<button type="button" class="btn btn-success m10t" onclick="reload();">Refresh info</button>
	</div>
</body>
</html>
