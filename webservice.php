<?php
header('Content-type: text/json');

$username = "marc"; 
$password = "degustabox";	

// Create connection
$db = mysqli_connect("localhost",$username,$password,"degustabox");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$result = array('result' => false, 'message' => 'There was an error, please try again later.');

$task = $_POST['task']; //name of task
$time = $_POST['time']; //duration of task
$date = date('Y-m-d');

//fist we have to see if already exists a task with the same name
$stmt = $db->prepare('SELECT * FROM tracker WHERE task = ? AND day = ? LIMIT 1');
$stmt->bind_param('ss', $task, $date); //escape string to prevent sql injection
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();

if ($row != NULL){ //update row

	$stmt = $db->prepare('UPDATE tracker SET duration = duration + ? WHERE task = ? AND day = ? LIMIT 1');
	$stmt->bind_param('dss', $time, $task, $date);

	$stmt->execute();
	$result = array('result' => true, 'message' => 'Information updated correctly');

} else { //insert new task

	$stmt = $db->prepare('INSERT INTO tracker (task, duration, day) VALUES (?,?,?)');
	$stmt->bind_param('sds', $task, $time, $date);

	$stmt->execute();
	$result = array('result' => true, 'message' => 'Information saved correctly');
}

echo json_encode($result);
