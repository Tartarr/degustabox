//javascipt code for exercise
var i = 0;
var time;
var task = '';
$( document ).ready(function() {

    $('#stopper').click(function(){  
    	/*mandamos ajax*/
    	$.ajax({
            url: '../webservice.php',
            type: 'POST',
            data: { 
            	task: $('#task').val(), 
            	time: i 
            },
            dataType: 'json',
            success: function(data) {
                if (data.result) {
                    $('#feedback-alert').addClass('alert-success').html('Your task has been saved.').css('display', 'block');
                } else {
                    $('#feedback-alert').addClass('alert-danger').html(data.message).css('display', 'block');
                }
            },
            error: function(data){
                $('#feedback-alert').addClass('alert-danger').html('There was an error, please try again later.').css('display', 'block');
            },
            complete: function(){
                //$('#submit').removeAttr('disabled');
            }
        });
        clearInterval(time);
        $('#starter').show();
    	$('#stopper').hide();
    });

    $('#starter').click(function(){
    	var name = $('#task').val();
    	if (name != ''){
    		if (name != task && task != '') {
    			reset();
    		}
          	task = name;
        	$('#starter').hide();
        	$('#stopper').show();
          	clearInterval(time);
          	run();
    	}else{
    		alert('You must write a valid task');
    	}
    });
    
});

function reload(){
    window.location.reload();
}
function run(){
    time = setInterval(function(){
        //$('#count').val(++i);
        $('#count').html(++i);
    },1000);        
}

function reset(){
        clearInterval(time);    
    i=0;
    $('#count').html(i);
    run();
}
